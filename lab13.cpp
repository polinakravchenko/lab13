﻿// lab13.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>

using namespace std;

class Mobile1
{
private:

    string model;
    string country;
    int year;
    double cost;
    int number;  
             
public:

    Mobile1()                    
    {

        model = "Sumsung A30";
        country = "South Korea";
        year = 2019;
        cost = 283.3;
        number = 544674;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "Country of origin of the phone:  " << country << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};
class Mobile2
{
private:

    string model;
    string country;
    int year;
    double cost;
    int number;

public:

    Mobile2()
    {

        model = "Xiamomi 11T";
        country = "China";
        year = 2021;
        cost = 600;
        number = 318527;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "Country of origin of the phone:  " << country << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

class Mobile3
{
private:

    string model;
    string country;
    int year;
    double cost;
    int number;

public:

    Mobile3()
    {

        model = "Apple Iphone 13ProMax";
        country = "Taiwan";
        year = 2021;
        cost = 1400;
        number = 318463;
    }
    void print_info()
    {
        cout << "The model of phone: " << model << endl;
        cout << "Country of origin of the phone:  " << country << endl;
        cout << "Year of manufacture: " << year << endl;
        cout << "The cost of the phone (dollars): " << cost << endl;
        cout << "The serial number of the phone: " << number << endl;

        cout << "============================== " << endl;
    }
};

int main()
{
    Mobile1 Tony;
    Tony.print_info();

    Mobile2 Jack;
    Jack.print_info();

    Mobile3 Hary;
    Hary.print_info();

    return 0;
}
